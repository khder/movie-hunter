﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using odev3;
using System.Data.Entity;

namespace odev3.Areas.Admin.Controllers
{
    public class AdminHomeController : AdminController
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            Movie_ReviewDBEntities db = new Movie_ReviewDBEntities();
            List<Tasks_tbl> tasks = db.Tasks_tbl.ToList();
            return View(tasks);
        }
    }
}